# BotNetter

```
Usage:
    tsamvgroup52 [comm_port] [info_port]
Info:
    [comm_port]    Port used for communication
    [info_port]    Port used for information
```

## Build Instructions

```sh
mkdir build && cd build
cmake ..
make
```

## Example

Using skel Makefile: `./tsamvgroup52 8888 8889`
USing cmake: `./BotNetterServer 8888 8889`

## Network Snapshots

Note: output is better formatted here.

Went a bit overboard in getting snapshots.

```
                V_GROUP_57;     V_GROUP_84;     V_Group_52;
V_Group_52;     V_GROUP_84;     V_Group_52;     -;              1540775558
```

```
                V_GROUP_84;     V_Group_52;
V_Group_52;     V_Group_52;     -;              1540777167
```

```
                V_GROUP_57;     V_GROUP_84;     V_Group_52;
V_Group_52;     V_Group_52;     V_Group_52;     -;              1540777745
```

```
                V_GROUP_4;      V_Group_52;     instructor_A;   instructor_B;   instructor_hidden;
V_Group_52;     V_Group_52;     -;              V_GROUP_4;      V_GROUP_4;      V_GROUP_4-instructor_B;     1540814091
```

```
                I_A;            I_B;    I_C;            I_D;            V_GROUP_15;     V_GROUP_4;      V_Group_52;     instructor_test;        server2;
V_Group_52;     V_Group_52;     I_A;    I_A-I_B;        I_A-I_B;        V_Group_52;     I_A;            -;              I_A;                    I_A;        1540835047
```

```
                I_A;           I_B;            I_C;                    I_D;                    I_E;                    V_GROUP_4;      V_GROUP_42;     V_Group_52;     instructor_test;        server2;
V_Group_52;     V_Group_52;    V_GROUP_4-I_B;  V_GROUP_4-I_B-I_C;      V_GROUP_4-I_B-I_D;      V_GROUP_4-I_B-I_C-I_E;  V_Group_52;     V_Group_52;     -;              I_A-instructor_test;    V_GROUP_4-server2;      1540835745
```

```
                I_A;            I_B;            I_C;                    I_D;                    I_E;                    V_GROUP_4;      V_GROUP_57;             V_Group_52;     instructor_test;
V_Group_52;     V_Group_52;     V_GROUP_4-I_B;  V_GROUP_4-I_B-I_C;      V_GROUP_4-I_B-I_D;      V_GROUP_4-I_B-I_C-I_E;  V_Group_52;     V_GROUP_4-V_GROUP_57;   -;              I_A-instructor_test;    1540837427
```

```
                I_A;            I_B;            I_C;            I_D;            I_E;            V_GROUP_15;     V_GROUP_4;      V_Group_52;     instructor_test;
V_Group_52;     V_Group_52;     I_C-I_B;        V_Group_52;     I_C-I_B-I_D;    I_C-I_E;        I_C-V_GROUP_15; V_Group_52;     -;              I_A-instructor_test;    1540837523
```

```
                I_A;            I_B;            I_C;            I_D;            I_E;            V_GROUP_12;     V_GROUP_4;      V_Group_52;     instructor_test;
V_Group_52;     V_Group_52;     I_C-I_B;        V_Group_52;     I_C-I_E-I_D;    I_C-I_E;        I_A-V_GROUP_12; V_Group_52;     -;              I_A-instructor_test;    1540837600
```

```
                I_A;            I_B;            I_C;            I_D;            I_E;            V_GROUP_4;      V_Group_52;     V_Group_69;     instructor_test;
V_Group_52;     V_Group_52;     V_Group_52;     I_B-I_C;        I_B-I_D;        I_B-I_C-I_E;    I_B-V_GROUP_4;  -;              I_A-V_Group_69; I_A-instructor_test;    1540840786
```

## Routing Snapshot When Hosted Outside of skel

V_GROUP_52 is hosted on skel, while V_GROUP_52H is hosted at home.

```
                I_D;            I_E;                    V_GROUP_4;      V_Group_52;     V_Group_52H;    instructor_test;
V_Group_52H;    V_GROUP_4-I_D;  V_GROUP_4-I_D-I_E;      V_Group_52H;    V_Group_52H;    -;              V_GROP_4-instructor_test;      1540850269
```

```
                I_D;            I_E;                    V_GROUP_4;      V_Group_52;     V_Group_52H;    V_Group_69 4069;
V_Group_52H;    V_GROUP_4-I_D;  V_GROUP_4-I_D-I_E;      V_Group_52H;    V_Group_52H;    -;              V_GROUP_4-I_D-V_Group_69 4069;  1540850717
```

## Chat Transcript

The wireshark logs are under the log directory named after the chat they correspond to.

### Chat 1

There is no wireshark log for this one but there is one for the other 2

```
CMD,V_GROUP_4B,V_Group_52,MSG ALL msg test
V_GROUP_4B:
MSG msg test3
CMD,V_GROUP_4B,V_Group_52,MSG ALL fékk þetta jei
V_GROUP_4B:
MSG msgCMD
CMD,V_GROUP_4B,V_Group_52,MSG ALL fáið þið svörin mín?
V_GROUP_4B:
MSG Já er að fá svörin þín :)
CMD,V_GROUP_4B,V_Group_52,MSG ALL Sweeeet
V_GROUP_4B:
MSG msg test3CMD
CMD,V_GROUP_4B,V_Group_52,MSG ALL Ætli þetta sé ekki nóg fyrir bónusin?
V_GROUP_4B:
MSG manst að taka niður routing snapshotið
V_GROUP_4B:
MSG Það þarf að vera með
CMD,V_GROUP_4B,V_Group_52,MSG ALL jess geri það
V_GROUP_4B:
MSG Held að við séum í góðum málumkvöld
V_GROUP_4B:
MSG *eigið gott kvöld :)
V_GROUP_4B:
MSG voruði ekki komin með routing snapshotið?
CMD,V_GROUP_4B,V_Group_52,MSG ALL Jebb allt komið, sömuleiðis
```

Routing info for chat:

### Chat 2

Don't think we have matching logs on this one but added it for completeness.

```
                I_A;            I_B;            I_C;            I_D;            I_E;            V_GROUP_12;     V_GROUP_4;      V_GROUP_4B;     V_Group_52;     instructor_test;
V_Group_52;     V_Group_52;     V_Group_52;     I_B-I_C;        I_B-I_D;        I_B-I_C-I_E;    I_A-V_GROUP_12; I_A-V_GROUP_4;  V_Group_52;     -;              I_A-instructor_test;    1540843722
```

```
CMD,V_GROUP_4B,V_Group_52H,MSG hjello
V_GROUP_4B:
MSG Halloooo
CMD,V_GROUP_4B,V_Group_52H,MSG olræt er að taka logga núna
CMD,V_GROUP_4B,V_Group_52H,MSG sorrý með lúðaskapinn
V_GROUP_4B:
MSG ekki málið
CMD,V_GROUP_4B,V_Group_52H,MSG þá ætti
V_GROUP_4B:
MSG virðist virka fínt
CMD,V_GROUP_4B,V_Group_52H,MSG jess þá ætti þetta að vera komið
CMD,V_GROUP_4B,V_Group_52H,MSG V_GROUP_4B:
MSG flott er :)
CMD,V_GROUP_4B,V_Group_52H,MSG fær maður loksins að sofa
```

Routing table

```
                V_GROUP_4B;     V_Group_52;     V_Group_52H;
V_Group_52H;    V_Group_52H;    V_Group_52H;    -;              1540854162
```

### Chat 3

This one should have matching logs with group 4.

```
V_GROUP_4B:
MSG er þér sama þótt við gerum þetta einu sinni enn
V_GROUP_4B:
MSG gleymdi að við þurfum að vera með matching logga
V_GROUP_4B:
MSG þarf að logga aftur líka
CMD,V_GROUP_4B,V_Group_52H,MSG olræt
CMD,V_GROUP_4B,V_Group_52H,MSG þetta > facebook
V_GROUP_4B:
MSG tru
V_GROUP_4B:
MSG ok er að logga núna
CMD,V_GROUP_4B,V_Group_52H,MSG ait
V_GROUP_4B:
MSG mindesCMD
V_GROUP_4B:
MSG lorem ipsum dolor lalala
V_GROUP_4B:
MSG random chat stuff
CMD,V_GROUP_4B,V_Group_52H,þvílikt og annað eins spjall maður
V_GROUP_4B:
MSG ok held að þetta ætti að vera fínt :)
CMD,V_GROUP_4B,V_Group_52H,olræt takk fyrir þetta
V_GROUP_4B:
MSG kominn með þitt?
CMD,V_GROUP_4B,V_Group_52H,jebb ætti að vera komið
V_GROUP_4B:
MSG ok segjum það :)
```

Routing table

```
                V_GROUP_4B;     V_Group_52;     V_Group_52H;
V_Group_52H;    V_Group_52H;    V_Group_52H;    -;              1540854479
```
