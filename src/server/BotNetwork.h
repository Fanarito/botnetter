#ifndef BOTNETTER_BOTNETWORK_H
#define BOTNETTER_BOTNETWORK_H

#include <map>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include <unordered_set>
#include <queue>
#include <stack>
#include <deque>

struct Bot
{
    std::string groupId;
    std::string address;
    std::string commPort;

    std::string str()
    {
        return groupId + "," + address + "," + commPort + ";";
    };
};

struct Node
{
    int dist;
    std::string id;
    std::string prev;
};

class BotNetwork
{
  private:
    std::unordered_set<std::string> allIds;
    std::map<std::string, std::vector<Bot>> botConnectionMap;
    std::unordered_map<std::string, Node *> getShortestPaths(
        std::string rootId);
    std::unordered_map<std::string, Node *> getShortestPath(
        std::string rootId,
        std::string targetId);
    std::deque<std::string> getShortestRoute(
        std::string rootId,
        std::string targetId,
        std::unordered_map<std::string, Node *> &nodes);

  public:
    void remove(std::string id);
    void add(std::string id, std::vector<Bot> connectedBots);
    void rebuildIds();

    /**
     * Gets all bots in network
     */
    std::vector<Bot> getConnectedBots();
    /**
     * Gets all bots connected to given bot
     */
    std::vector<Bot> getConnectedBots(std::string botId);
    /**
     * Get routing table from node
     */
    std::string getRoutingTable(std::string rootId);
    /**
     * Get shortest path to node
     */
    std::deque<std::string> getShortestRoute(std::string rootId, std::string targetId);
    /**
     * Returns true if the network contains that node
     */
    bool containsBot(std::string botId);
    /**
     * Returns all server ids known in network
     */
    std::unordered_set<std::string> getAllIds();
};

#endif // BOTNETTER_BOTNETWORK_H