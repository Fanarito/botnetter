#include <iostream>

#include "./Server.h"
#include "../shared/BotNetter.hpp"
#include "../shared/RandUtils.hpp"
#include "../shared/StringUtils.hpp"

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "Usage:" << std::endl
                  << "    BotNetterServer [comm_port] [info_port]" << std::endl
                  << "Info:" << std::endl
                  << "    [comm_port]    Port used for communication" << std::endl
                  << "    [info_port]    Port used for information" << std::endl;
        return 0;
    }

    ServerOptions serverOptions;
    serverOptions.commPort = argv[1];
    serverOptions.infoPort = argv[2];
    Server server(serverOptions);
    server.start();
}