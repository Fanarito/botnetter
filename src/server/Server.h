#ifndef BOTNETTER_SERVER_H
#define BOTNETTER_SERVER_H

#include <iostream>
#include <vector>
#include <thread>
#include <future>
#include <chrono>
#include <map>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>

#include "BotNetwork.h"

#include "../shared/BotNetter.hpp"
#include "../shared/NetworkUtils.h"
#include "../shared/StringUtils.hpp"

struct Client
{
    int sock;
    std::string username;
    std::string address;
    std::string commPort;
};

// Inhereting from bot currently breaks runtime no clue why
struct ConnectedServer : Bot
{
    std::string infoPort;
    int sock;
    long lastKeepalive;
};

class Server
{
  private:
    std::string id = "V_Group_52";
    ConnectedServer thisServer;

    BotNetwork botNetwork;

    ServerOptions serverOptions;
    int commSocket, infoSocket;
    std::map<int, ConnectedServer> connectedServers;
    std::map<int, Client> clients;
    std::map<int, std::string> socketMessageMap;

    fd_set master;
    fd_set readFds;

    int fdmax;

    std::thread commProcessThread;
    std::thread infoProcessThread;
    std::thread keepAliveThread;
    std::thread cleanupThread;
    std::thread probeThread;

    void commProcess();
    void infoProcess();
    void keepAliveProcess();
    void cleanupProcess();
    void probeProcess();

    void probeNetwork();

    std::string getListServersMsg();

    /**
     * Responds to the client with a list of all connected servers.
     * @param sock
     */
    void listServers(int sock);

    /**
     * Sends a keepalive message to all connected servers.
     */
    void keepAlive();

    /**
     * Broadcasts message to all clients
     * @param message
     */
    void broadcast(std::string message);

    Client *getClient(int sock);

    Client *getClient(std::string username);

    ConnectedServer *getServer(std::string id);
    ConnectedServer *getServer(int sock);

    /**
     * Sends all users on server
     * @param sender
     * @param message
     */
    void sendAll(std::string message);

    /**
     * Ends connection with client
     * @param client
     * @param invalidCommand
     */
    void endConnection(Client client, bool invalidCommand = false);
    void endConnection(int sock);

    void processRawMessage(Client *sender, std::string message);
    /**
     * Processes message and does all the work
     * @param sender
     * @param message
     */
    void processMessage(Client *sender, std::string message);

    void cmdProcessor(Client *sender, std::vector<std::string> tokens);

    void rspProcessor(Client *sender, std::vector<std::string> tokens);

    std::string responseBuilder(std::string recipientId, std::string senderId,
                                std::vector<std::string> commandTokens);

    void joinNetwork(std::string address, std::string commPort, std::string infoPort);

    void routeServerMessage(std::string targetId, std::string message);

  public:
    Server(ServerOptions serverOptions);

    void start();
};

#endif // BOTNETTER_SERVER_H
