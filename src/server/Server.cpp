#include "Server.h"

Server::Server(ServerOptions serverOptions) : serverOptions(serverOptions)
{
    // Temporary
    // id = serverOptions.commPort;

    // Set the information for this server
    thisServer.address = NetworkUtils::getLocalAddress();
    thisServer.groupId = id;
    thisServer.commPort = serverOptions.commPort;
    thisServer.infoPort = serverOptions.infoPort;
}

void Server::start()
{
    commSocket = NetworkUtils::openSocket(std::atoi(serverOptions.commPort.c_str()), SOCK_STREAM);
    infoSocket = NetworkUtils::openSocket(std::atoi(serverOptions.infoPort.c_str()), SOCK_DGRAM);

    commProcessThread = std::thread(&Server::commProcess, this);
    infoProcessThread = std::thread(&Server::infoProcess, this);
    keepAliveThread = std::thread(&Server::keepAliveProcess, this);
    cleanupThread = std::thread(&Server::cleanupProcess, this);
    probeThread = std::thread(&Server::probeProcess, this);

    commProcessThread.join();
    infoProcessThread.join();
    keepAliveThread.join();
    cleanupThread.join();
    probeThread.join();
}

void Server::commProcess()
{
    printf("%s", "started comm process\n");

    if (listen(commSocket, 6) == -1)
    {
        perror("listen");
        exit(3);
    }

    int newfd;
    sockaddr_storage remoteaddr{};
    socklen_t addrlen;

    char buf[BUFFER_SIZE];

    char remoteIP[INET6_ADDRSTRLEN];

    int rv;

    FD_ZERO(&master);
    FD_ZERO(&readFds);

    // Add listener to master set
    FD_SET(commSocket, &master);

    // Keep track of the biggest file descriptor
    fdmax = commSocket;

    // Receive loop
    for (;;)
    {
        bzero(buf, sizeof(buf));
        readFds = master;

        // Wait for activity in set
        if (select(fdmax + 1, &readFds, nullptr, nullptr, nullptr) == -1)
        {
            perror("select");
            exit(4);
        }

        // Run through all connections and check for activity and
        // handle it if there is any.
        for (int i = 0; i <= fdmax; i++)
        {
            if (FD_ISSET(i, &readFds))
            {
                if (i == commSocket)
                {
                    // If there is activity on the listener socket there is a new connection
                    addrlen = sizeof remoteaddr;
                    newfd = accept(commSocket, (sockaddr *)&remoteaddr, &addrlen);
                    if (newfd == -1)
                    {
                        perror("accept");
                        exit(5);
                    }
                    else
                    {
                        // Connection created, setup all needed data.
                        FD_SET(newfd, &master);
                        // Keep track of the max
                        if (newfd > fdmax)
                        {
                            fdmax = newfd;
                        }
                        printf("selectserver: new connection from %s on socket %d\n",
                               inet_ntop(remoteaddr.ss_family,
                                         NetworkUtils::getInAddr((struct sockaddr *)&remoteaddr),
                                         remoteIP, INET6_ADDRSTRLEN),
                               newfd);

                        Client client;
                        client.sock = newfd;
                        client.username = "default" + std::to_string(newfd);
                        client.address = remoteIP;
                        client.commPort = std::to_string(ntohs(((sockaddr_in *)&remoteaddr)->sin_port));
                        clients.insert(std::pair<int, Client>(client.sock, client));

                        std::cout << "new connection: " << client.address << ":" << client.commPort << std::endl;
                    }
                }
                else
                {
                    // In this case there is a message from a client, either
                    // a new message or a closed connection
                    int nbytes;
                    if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0)
                    {
                        // Handle closed connection
                        if (nbytes == 0)
                        {
                            printf("selectserver: socket %d hung up\n", i);
                        }
                        else
                        {
                            perror("recv");
                        }
                        endConnection(clients.find(i)->second.sock);
                    }
                    else
                    {
                        // Handle received message
                        std::async(std::launch::async, [&] {
                            Client *sender = getClient(i);
                            processRawMessage(sender, buf);
                        });
                    }
                }
            }
        }
    }
}

void Server::keepAliveProcess()
{
    printf("%s", "started keepalive process\n");
    for (;;)
    {
        std::this_thread::sleep_for(std::chrono::seconds(60));
        keepAlive();
    }
}

void Server::cleanupProcess()
{
    printf("%s", "started cleanup process\n");
    for (;;)
    {
        bool anyChange = false;
        std::this_thread::sleep_for(std::chrono::seconds(120));
        long now = time(nullptr);
        for (auto it = connectedServers.begin(); it != connectedServers.end(); ++it)
        {
            auto &server = *it;
            long diff = now - server.second.lastKeepalive;
            if (diff > 180)
            {
                std::cout << "Removing " << server.second.groupId
                          << " because of lack of keep alives" << std::endl;
                endConnection(server.second.sock);
                anyChange = true;
            }
        }

        if (anyChange)
            probeNetwork();
    }
}

void Server::probeProcess()
{
    printf("%s", "started keepalive process\n");
    for (;;)
    {
        std::this_thread::sleep_for(std::chrono::seconds(30));
        probeNetwork();
    }
}

void Server::infoProcess()
{
    printf("%s", "started info process\n");
    sockaddr_in theirAddr;
    socklen_t addrLen;
    int numBytes = -1;
    char buffer[2048];
    char addressStr[INET6_ADDRSTRLEN];

    while (1)
    {
        if (recvfrom(infoSocket, buffer, sizeof(buffer) - 1, 0,
                     (sockaddr *)&theirAddr, &addrLen) == -1)
        {
            perror("recvfrom");
            continue;
        }

        std::string msg = getListServersMsg();
        if (sendto(infoSocket, msg.c_str(), msg.length(), 0,
                   (sockaddr *)&theirAddr, addrLen) == -1)
        {
            perror("sendto");
            continue;
        }
    }
}

void Server::probeNetwork()
{
    std::vector<Bot> bots;
    bots.reserve(connectedServers.size());
    std::for_each(connectedServers.begin(), connectedServers.end(),
                  [&](const std::map<int, ConnectedServer>::value_type &s) {
                      bots.push_back(s.second);
                  });
    botNetwork.add(id, bots);

    for (auto &serverId : botNetwork.getAllIds())
    {
        if (serverId == id)
            continue;
        std::string msg = "CMD," + serverId + "," + id + ",LISTSERVERS";
        routeServerMessage(serverId, msg);
    }
}

ConnectedServer *Server::getServer(std::string serverId)
{
    for (auto &s : connectedServers)
    {
        if (s.second.groupId == serverId)
        {
            return &s.second;
        }
    }
    return nullptr;
}

ConnectedServer *Server::getServer(int sock)
{
    auto serverIt = connectedServers.find(sock);
    if (serverIt == connectedServers.end())
    {
        return nullptr;
    }
    return &serverIt->second;
}

std::string Server::getListServersMsg()
{
    std::string msg;
    for (auto &&server : connectedServers)
    {
        msg.append(server.second.str());
    }
    return msg;
}

void Server::listServers(int sock)
{
    std::string msg = getListServersMsg();
    NetworkUtils::sendMsg(sock, msg);
}

void Server::keepAlive()
{
    std::string msg = "KEEPALIVE";
    for (auto &&server : connectedServers)
    {
        NetworkUtils::sendMsg(server.first, msg);
    }
}

void Server::broadcast(std::string message)
{
    message.insert(0, "\01");
    message.append("\04");
    for (auto &client : clients)
    {
        int fd = client.first;
        if (FD_ISSET(fd, &master) && getServer(fd) == nullptr)
        {
            NetworkUtils::sendMsg(fd, message);
        }
    }
}

Client *Server::getClient(int sock)
{
    return &clients.find(sock)->second;
}

Client *Server::getClient(std::string username)
{
    for (auto &client : clients)
    {
        if (client.second.username == username)
        {
            return &client.second;
        }
    }
    return nullptr;
}

void Server::sendAll(std::string message)
{
    broadcast(std::move(message));
}

void Server::processRawMessage(Client *sender, std::string message)
{
    auto msgFragmentIt = socketMessageMap.find(sender->sock);
    std::string msgFragment;
    if (msgFragmentIt != socketMessageMap.end())
        msgFragment = msgFragmentIt->second;

    std::string::iterator lastStart;
    for (auto it = message.begin(); it != message.end(); ++it)
    {
        if (*it == '\01')
            lastStart = it;
        else if (*it == '\04')
        {
            // We add one to lastStart to skip the msg start token
            msgFragment.insert(msgFragment.end(), lastStart + 1, it);
            StringUtils::trim(msgFragment);
            processMessage(sender, msgFragment);
            msgFragment.clear();
        }
    }
    msgFragment.insert(msgFragment.end(), message.begin(), message.end());
}

void Server::processMessage(Client *sender, std::string message)
{
    // Echo out valid messages
    std::stringstream ss;
    ss << "(" << sender->address << ":" << sender->commPort << "): " << message << std::endl;
    std::cout << ss.str();

    std::vector<std::string> tokens = StringUtils::split(message, COMMAND_DELIM);

    if (tokens.empty())
    {
        return;
    }
    std::string command = tokens.at(0);

    if (std::strncmp(message.c_str(), "MSG", 3) == 0)
    {
        std::string senderId = sender->username;
        auto server = getServer(sender->sock);
        if (server != nullptr)
            senderId = server->groupId;
        std::string to_send = senderId + ":\n" + tokens.at(2);
        sendAll(to_send);
    }
    else if (command == "WHO")
    {
        std::string to_send;
        for (auto &user : clients)
        {
            to_send.append(user.second.username);
            to_send.append("\n");
        }
        to_send.pop_back();
        NetworkUtils::sendMsg(sender->sock, to_send);
    }
    else if (command == "LEAVE")
    {
        std::string to_send = "Goodbye\n";
        NetworkUtils::sendMsg(sender->sock, to_send);
        endConnection(*sender);
    }
    else if (command == "CONNECT")
    {
        std::string username = tokens.at(1);
        std::string to_send = "Username: " + username;
        sender->username = username;
        NetworkUtils::sendMsg(sender->sock, to_send);
    }
    else if (command == "ID")
    {
        std::string to_send = id;
        NetworkUtils::sendMsg(sender->sock, to_send);
    }
    else if (command == "SHOWNETWORK")
    {
        // Display network
        std::stringstream msg;
        for (auto &bot : botNetwork.getConnectedBots())
        {
            msg << bot.str() << std::endl;
        }
        NetworkUtils::sendMsg(sender->sock, msg.str());
    }
    else if (command == "PROBE")
    {
        probeNetwork();
    }
    else if (command == "JOIN")
    {
        if (tokens.size() < 4)
        {
            return;
        }
        std::string address = tokens.at(1);
        std::string commPort = tokens.at(2);
        std::string infoPort = tokens.at(3);
        joinNetwork(address, commPort, infoPort);
    }
    else if (command == "LISTSERVERS")
    {
        listServers(sender->sock);
    }
    else if (command == "CMD")
    {
        cmdProcessor(sender, tokens);
    }
    else if (command == "RSP")
    {
        rspProcessor(sender, tokens);
    }
    else if (command == "KEEPALIVE")
    {
        ConnectedServer *server = getServer(sender->sock);
        if (server != nullptr)
        {
            server->lastKeepalive = time(nullptr);
        }
    }
    else if (command == "LISTROUTES")
    {
        NetworkUtils::sendMsg(sender->sock, botNetwork.getRoutingTable(id));
    }
    else
    {
        // Invalid command
        std::string msg = "Invalid command";
        NetworkUtils::sendMsg(sender->sock, msg);
    }
}

void Server::cmdProcessor(Client *sender, std::vector<std::string> tokens)
{
    if (tokens.size() < 4)
    {
        // not enough tokens for correct command
        return;
    }

    std::string recipientId = tokens.at(1);
    std::string senderId = tokens.at(2);
    std::string command = tokens.at(3);
    std::vector<std::string> commandTokens(tokens.begin() + 3, tokens.end());

    if (recipientId.empty() && command == "ID")
    {
        // Unrecognized server attempting to connect
        // we allow it to connect if there is space otherwise
        // we drop the connection, sorry server
        if (connectedServers.size() > 5)
        {
            endConnection(*sender);
        }

        ConnectedServer newServer;
        newServer.groupId = senderId;
        newServer.address = sender->address;
        newServer.sock = sender->sock;
        newServer.lastKeepalive = time(nullptr);

        // If the server is already in our list we have connected
        // to the same server twice. End this connection.
        if (getServer(senderId) != nullptr)
        {
            std::cout << "Ending duplicate connection" << std::endl;
            endConnection(sender->sock);
            return;
        }

        connectedServers.emplace(sender->sock, newServer);

        // We don't know their id so we ask for it
        std::string msg = "CMD," + senderId + "," + id + ",ID";
        NetworkUtils::sendMsg(sender->sock, msg);
        // return;
    }

    if (recipientId.empty())
        // This message was probs meant for us
        recipientId = id;

    if (recipientId == id)
    {
        // We don't know the sender, attempt to add him to our network
        std::cout << botNetwork.getAllIds().size() << std::endl;
        if (!botNetwork.containsBot(senderId))
        {
            std::cout << "sender not in network, attempting to add it" << std::endl;
            std::cout << botNetwork.getAllIds().size() << " " << botNetwork.containsBot(senderId) << std::endl;
            std::string msg = "CMD," + senderId + "," + id + ",LISTSERVERS";
            NetworkUtils::sendMsg(sender->sock, msg);
        }

        if (command == "KEEPALIVE")
        {
            ConnectedServer *server = getServer(sender->sock);
            if (server != nullptr)
            {
                server->lastKeepalive = time(nullptr);
            }
        }

        if (std::strncmp(command.c_str(), "MSG", 3) == 0)
        {
            std::string senderId = sender->username;
            auto server = getServer(sender->sock);
            if (server != nullptr)
                senderId = server->groupId;
            std::string to_send = senderId + ":\n" + command;
            sendAll(to_send);
            return;
        }

        std::string msg = responseBuilder(recipientId, senderId, commandTokens);
        if (msg != "")
            NetworkUtils::sendMsg(sender->sock, msg);
    }
    else
    {
        routeServerMessage(recipientId, StringUtils::join(tokens, COMMAND_DELIM));
    }
}

void Server::routeServerMessage(std::string targetId, std::string message)
{
    std::cout << "Attempting to route to " << targetId << std::endl;

    auto server = getServer(targetId);
    if (server == nullptr)
    {
        if (botNetwork.containsBot(targetId))
        {
            auto shortestRoute = botNetwork.getShortestRoute(id, targetId);

            // If the shortest route begins with the targetId
            // we know there is no known route.
            if (!shortestRoute.empty() && shortestRoute.front() != targetId && shortestRoute.front() != "")
            {
                // Route starts with us so we get second jump in route
                server = getServer(shortestRoute.at(1));
                // Display route
                std::cout << "attempting to display route" << std::endl;
                while (!shortestRoute.empty())
                {
                    std::cout << shortestRoute.front() << " -> ";
                    shortestRoute.pop_front();
                }
                std::cout << std::endl;
            }
            else
            {
                std::cout << "NO ROUTE FOUND REMOVING " << targetId << std::endl;
                botNetwork.remove(targetId);
            }
        }
    }

    if (server != nullptr)
    {
        std::cout << "Route found sending to " << server->groupId << std::endl;
        NetworkUtils::sendMsg(server->sock, message);
    }
    else
    {
        // Not in network
        std::cout << "Trying to send to node not in network will probe and drop msg" << std::endl;
    }
}

void Server::rspProcessor(Client *sender, std::vector<std::string> tokens)
{
    std::string recipientId = tokens.at(1);
    std::string senderId = tokens.at(2);
    std::string command = tokens.at(3);
    std::vector<std::string> responseTokens(tokens.begin() + 4, tokens.end());

    // Some servers send empty recipient id in response???
    // most of the time it is meant for us so we just
    // pretend it had the id the whole time.
    if (recipientId == "")
        recipientId = id;

    if (recipientId == id)
    {
        if (command == "ID")
        {
            ConnectedServer *server = getServer(sender->sock);
            if (responseTokens.size() == 2)
            {
                server->groupId = responseTokens.at(0);
            }
            else
            {
                server->groupId = responseTokens.at(0);
                server->address = responseTokens.at(1);
                server->commPort = StringUtils::split(responseTokens.at(2), ';').at(0);
            }
            // Most likely a final connection handshake response
            // so we probe the network.
            probeNetwork();
        }
        else if (command == "LISTSERVERS")
        {
            auto lsRsp = StringUtils::join(responseTokens, COMMAND_DELIM);
            auto serverStrings = StringUtils::split(lsRsp, ';');
            std::vector<Bot> connectedBots;
            for (auto &serverString : serverStrings)
            {
                auto serverInfo = StringUtils::split(serverString, COMMAND_DELIM);
                Bot bot;
                bot.groupId = serverInfo.at(0);
                bot.address = serverInfo.at(1);
                bot.commPort = serverInfo.at(2);
                if (bot.groupId != "")
                    connectedBots.push_back(bot);

                if (!botNetwork.containsBot(bot.groupId))
                {
                    if (bot.groupId != "")
                    {
                        std::cout << "Attempting to add " << bot.groupId << " to network" << std::endl;
                        std::string msg = "CMD," + senderId + "," + id + ",LISTSERVERS";
                        NetworkUtils::sendMsg(sender->sock, msg);
                    }
                }
            }
            botNetwork.add(senderId, connectedBots);
        }
    }
    else
    {
        routeServerMessage(recipientId, StringUtils::join(tokens, COMMAND_DELIM));
    }
}

std::string Server::responseBuilder(std::string recipientId, std::string senderId,
                                    std::vector<std::string> commandTokens)
{
    std::string command = commandTokens.at(0);
    std::stringstream response;

    response << "RSP," + senderId << "," << recipientId << "," << command << ",";

    if (command == "WHO")
    {
        std::string to_send;
        for (auto &user : clients)
        {
            to_send.append(user.second.username);
            to_send.append("\n");
        }
        to_send.pop_back();
        response << to_send;
    }
    else if (command == "ID")
    {
        std::string msg = thisServer.str();
        msg.pop_back();
        response << msg;
    }
    else if (command == "LISTSERVERS")
    {
        response << getListServersMsg();
    }
    else if (command == "LISTROUTES")
    {
        response << botNetwork.getRoutingTable(id);
    }
    else if (command == "FETCH")
    {
        std::string hashes[] = {
            "hash1", "hash2", "hash3", "hash4", "hash5"};
        if (commandTokens.size() > 1)
        {
            int num = std::atoi(commandTokens.at(1).c_str()) - 1;
            if (num <= 4)
            {
                response << hashes[num];
            }
        }
    }
    else
    {
        return "";
    }

    return response.str();
}

void Server::joinNetwork(std::string address, std::string commPort, std::string infoPort)
{
    struct addrinfo hints, *svr; // Network host entry for server
    int set = 1;

    memset(&hints, 0, sizeof(hints));

    hints.ai_family = AF_INET; // IPv4 only addresses
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(address.c_str(), commPort.c_str(), &hints, &svr) != 0)
    {
        perror("getaddrinfo failed: ");
        exit(0);
    }

    int sock = socket(svr->ai_family, svr->ai_socktype, svr->ai_protocol);

    // Turn on SO_REUSEADDR to allow socket to be quickly reused after
    // program exit.
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &set, sizeof(set)) < 0)
    {
        printf("Failed to set SO_REUSEADDR for port %s\n", commPort.c_str());
        perror("setsockopt failed: ");
    }

    if (connect(sock, svr->ai_addr, svr->ai_addrlen) < 0)
    {
        printf("Failed to open socket to server: %s\n", address.c_str());
        perror("Connect failed: ");
        return;
    }

    // Add newly created socket to master set
    FD_SET(sock, &master);
    if (sock > fdmax)
    {
        fdmax = sock;
    }

    Client client;
    client.sock = sock;
    client.username = "default" + std::to_string(sock);
    client.address = address;
    client.commPort = commPort;
    clients.insert(std::pair<int, Client>(client.sock, client));

    ConnectedServer newServer;
    newServer.address = address;
    newServer.commPort = commPort;
    newServer.infoPort = infoPort;
    newServer.sock = sock;
    newServer.lastKeepalive = time(nullptr);
    connectedServers.emplace(sock, newServer);

    std::string msg = "CMD,," + id + ",ID";
    NetworkUtils::sendMsg(newServer.sock, msg);
}

void Server::endConnection(Client client, bool invalidCommand)
{
    std::cout << "(" << client.sock << "," << client.username << "): disconnected "
              << invalidCommand << std::endl;
    // TODO remove disconnected servers
    close(client.sock);
    FD_CLR(client.sock, &master); // remove from master set
    clients.erase(client.sock);
}

void Server::endConnection(int sock)
{
    close(sock);
    FD_CLR(sock, &master);

    // Remove sock related data
    Client *client = getClient(sock);
    if (client != nullptr)
        clients.erase(sock);
    ConnectedServer *server = getServer(sock);
    if (server != nullptr)
    {

        for (auto it = connectedServers.begin(); it != connectedServers.end(); ++it)
        {
            if (it->second.sock == sock)
            {
                botNetwork.remove(it->second.groupId);
                connectedServers.erase(it);
                break;
            }
        }
        botNetwork.rebuildIds();
        probeNetwork();
    }
}