#include "BotNetwork.h"

// TODO remove
#include <iostream>

void BotNetwork::remove(std::string id)
{
    botConnectionMap.erase(id);
    allIds.erase(id);
}

void BotNetwork::add(std::string id, std::vector<Bot> connectedBots)
{
    // Empty ids are baaaad
    if (id == "")
        return;
    botConnectionMap[id] = connectedBots;
    allIds.insert(id);

    for (auto &&bot : connectedBots)
    {
        if (bot.groupId != "")
            allIds.insert(bot.groupId);
    }
}

void BotNetwork::rebuildIds()
{
    allIds.clear();
    for (auto &b1 : botConnectionMap)
    {
        allIds.insert(b1.first);
        for (auto &b2 : b1.second)
        {
            allIds.insert(b2.groupId);
        }
    }
}

std::vector<Bot> BotNetwork::getConnectedBots()
{
    std::vector<Bot> bots;
    std::unordered_set<std::string> ids;

    for (auto &pair : botConnectionMap)
    {
        auto connectedBots = pair.second;
        for (auto bot : connectedBots)
        {
            if (ids.find(bot.groupId) != ids.end())
                continue;
            ids.insert(bot.groupId);
            bots.push_back(bot);
        }
    }

    return bots;
}

std::vector<Bot> BotNetwork::getConnectedBots(std::string botId)
{
    auto pair = botConnectionMap.find(botId);
    return pair->second;
}

std::string BotNetwork::getRoutingTable(std::string rootId)
{
    auto nodes = getShortestPaths(rootId);
    std::map<std::string, std::deque<std::string>> paths;

    // Cleanup pointers
    for (auto n : nodes)
    {
        auto route = getShortestRoute(rootId, n.first, nodes);
        paths.emplace(n.first, route);
    }

    std::stringstream ss;
    for (auto &p : paths)
    {
        ss << "\t" << p.first << ";";
    }
    ss << '\n';
    ss << rootId << ";\t";

    for (auto &p : paths)
    {
        if (p.first == rootId)
        {
            ss << "-;" << '\t';
            continue;
        }

        // Direct route we display our id
        if (p.second.size() == 2)
        {
            ss << p.second.at(0);
        }
        else
        {
            // Loop through route except first node and add to routing table
            for (auto it = p.second.begin() + 1; it != p.second.end(); ++it)
            {
                ss << *it;
                if (it + 1 != p.second.end())
                    ss << '-';
            }
        }
        ss << ";" << '\t';
    }

    // Add timestamp to message
    ss << time(nullptr);

    for (auto n : nodes)
        delete n.second;
    return ss.str();
}

std::deque<std::string> BotNetwork::getShortestRoute(
    std::string rootId,
    std::string targetId,
    std::unordered_map<std::string, Node *> &nodes)
{
    std::deque<std::string> route;
    int jumps = 0;

    auto u = targetId;
    // No route found
    if (nodes.find(u) == nodes.end())
    {
        std::cout << "NO ROUTE FOUND" << std::endl;
        return route;
    }

    if (nodes.at(u)->prev != "" || u == rootId)
    {
        while (u != "")
        {
            jumps++;
            route.push_front(u);
            u = nodes.at(u)->prev;

            // Hacky way to avoid thread sync
            if (jumps > allIds.size())
            {
                std::cout << "THREAD SYNC ISSUES PLS FIX" << std::endl;
                return std::deque<std::string>();
            }
        }
    }

    return route;
}

std::deque<std::string> BotNetwork::getShortestRoute(std::string rootId, std::string targetId)
{
    auto nodes = getShortestPath(rootId, targetId);
    return getShortestRoute(rootId, targetId, nodes);
}

std::unordered_map<std::string, Node *> BotNetwork::getShortestPaths(std::string rootId)
{
    // Lowest first priority queue
    auto comp = [](Node *a, Node *b) { return a->dist > b->dist; };
    std::vector<Node *> q;
    std::unordered_map<std::string, Node *> nodes;

    for (auto bot : getConnectedBots())
    {
        Node *n = new Node;
        n->dist = INT32_MAX;
        n->id = bot.groupId;

        if (bot.groupId == rootId)
            n->dist = 0;

        q.push_back(n);
        nodes[bot.groupId] = n;
    }

    std::make_heap(q.begin(), q.end(), comp);
    auto it = q.end();
    while (it != q.begin())
    {
        Node *u = q.front();
        std::pop_heap(q.begin(), it, comp);
        it--;

        for (auto vBot : botConnectionMap[u->id])
        {
            Node *v = nodes[vBot.groupId];
            int alt = u->dist + 1;
            if (alt < v->dist)
            {
                v->dist = alt;
                v->prev = u->id;
                std::make_heap(q.begin(), it, comp);
            }
        }
    }

    return nodes;
}

std::unordered_map<std::string, Node *> BotNetwork::getShortestPath(
    std::string rootId,
    std::string targetId)
{
    // Lowest first priority queue
    auto comp = [](Node *a, Node *b) { return a->dist > b->dist; };
    std::vector<Node *> q;
    std::unordered_map<std::string, Node *> nodes;

    for (auto bot : getConnectedBots())
    {
        Node *n = new Node;
        n->dist = INT32_MAX;
        n->id = bot.groupId;

        if (bot.groupId == rootId)
            n->dist = 0;

        q.push_back(n);
        nodes[bot.groupId] = n;
    }

    std::make_heap(q.begin(), q.end(), comp);
    auto it = q.end();
    while (it != q.begin())
    {
        Node *u = q.front();
        std::pop_heap(q.begin(), it, comp);
        it--;

        if (u->id == targetId)
            return nodes;

        for (auto vBot : botConnectionMap[u->id])
        {
            Node *v = nodes[vBot.groupId];
            int alt = u->dist + 1;
            if (alt < v->dist)
            {
                v->dist = alt;
                v->prev = u->id;
                std::make_heap(q.begin(), it, comp);
            }
        }
    }

    return nodes;
}

bool BotNetwork::containsBot(std::string botId)
{
    return allIds.find(botId) != allIds.end();
}

std::unordered_set<std::string> BotNetwork::getAllIds()
{
    return allIds;
}