#include "NetworkUtils.h"

// Needed because sometimes cpp is dumb
std::mutex NetworkUtils::sendMutex;

int NetworkUtils::openSocket(int port_no, int sock_type)
{
    struct sockaddr_in sk_addr; // address settings for bind()
    int sock;                   // socket opened for this port
    int set = 1;                // for setsockopt

    // Create socket for connection
    if ((sock = socket(AF_INET, sock_type, 0)) < 0)
    {
        perror("Failed to open socket");
        return (-1);
    }

    // Needed for apple for some reason
#ifdef APPLE
    fcntl(sock, F_SETFL, O_NONBLOCK);
#endif

    // Turn on SO_REUSEADDR to allow socket to be quickly reused after
    // program exit.
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &set, sizeof(set)) < 0)
    {
        perror("Failed to set SO_REUSEADDR:");
    }

    std::memset(&sk_addr, 0, sizeof(sk_addr));
    sk_addr.sin_family = AF_INET;
    sk_addr.sin_addr.s_addr = INADDR_ANY;
    sk_addr.sin_port = htons(port_no);

    // Bind to socket to listen for connections from clients
    if (bind(sock, (struct sockaddr *)&sk_addr, sizeof(sk_addr)) < 0)
    {
        perror("Failed to bind to socket:");
        return (-1);
    }
    return (sock);
}

void *NetworkUtils::getInAddr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET)
    {
        return &(((struct sockaddr_in *)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6 *)sa)->sin6_addr);
}

bool NetworkUtils::sendMsg(int sock, std::string msg)
{
    std::lock_guard<std::mutex> lock(sendMutex);
    std::cout << "Sending msg: " << msg << std::endl;
    msg.insert(0, "\01");
    msg.append("\04");
    if (send(sock, msg.c_str(), msg.length(), 0) == -1)
    {
        perror("sending message");
        return false;
    }
    return true;
}

std::string NetworkUtils::getLocalAddress()
{
    std::string address;
    struct ifaddrs *localAddress, *ifa;
    void *in_addr;
    char buf[64];

    if (getifaddrs(&localAddress) != 0)
    {
        perror("getifaddrs");
        exit(1);
    }

    for (ifa = localAddress; ifa != NULL; ifa = ifa->ifa_next)
    {
        if (ifa->ifa_addr == NULL)
            continue;
        if (!(ifa->ifa_flags & IFF_UP))
            continue;

        switch (ifa->ifa_addr->sa_family)
        {
        case AF_INET:
        {
            struct sockaddr_in *s4 = (struct sockaddr_in *)ifa->ifa_addr;
            in_addr = &s4->sin_addr;
            break;
        }

        case AF_INET6:
        {
            struct sockaddr_in6 *s6 = (struct sockaddr_in6 *)ifa->ifa_addr;
            in_addr = &s6->sin6_addr;
            break;
        }

        default:
            continue;
        }

        if (!inet_ntop(ifa->ifa_addr->sa_family, in_addr, buf, sizeof(buf)))
        {
            printf("%s: inet_ntop failed!\n", ifa->ifa_name);
        }
        else
        {
            printf("%s: %s\n", ifa->ifa_name, buf);
            address = buf;
        }
    }
    freeifaddrs(localAddress);
    return address;
}
