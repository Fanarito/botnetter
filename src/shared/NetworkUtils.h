#ifndef __NETWORK_UTILS_H_
#define __NETWORK_UTILS_H_

#include <mutex>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <stdio.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <arpa/inet.h>

class NetworkUtils
{
  private:
    static std::mutex sendMutex;

  public:
    static int openSocket(int port_no, int sock_type);
    static void *getInAddr(struct sockaddr *sa);
    static bool sendMsg(int sock, std::string msg);
    static std::string getLocalAddress();
};

#endif // __NETWORK_UTILS_H_
