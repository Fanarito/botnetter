#ifndef __BOT_NETTER_H_
#define __BOT_NETTER_H_

#include <string>

struct ServerOptions
{
    std::string commPort;
    std::string infoPort;
};

struct ServerCommands
{
    std::string
        ListServers = "LISTSERVERS",
        KeepAlive = "KEEPALIVE",
        ListRoutes = "LISTROUTES",
        CMD = "CMD",
        RSP = "RSP",
        MSG = "MSG",
        Fetch = "FETCH";
};

static const char COMMAND_DELIM = ',';
static const int BUFFER_SIZE = 1024;

#endif // __BOT_NETTER_H_
