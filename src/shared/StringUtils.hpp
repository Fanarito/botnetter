#ifndef __STRING_UTILS_H_
#define __STRING_UTILS_H_

#include <iterator>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
#include <locale>

class StringUtils
{
  public:
    template <typename Out>
    static void split(const std::string &s, char delim, Out result)
    {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim))
        {
            *(result++) = item;
        }
    }

    /**
     * Splits string by delimiter
     * @param s string to split
     * @param delim delimiter to split by
     * @return vector of strings
     */
    static std::vector<std::string> split(const std::string &s, char delim)
    {
        std::vector<std::string> elems;
        split(s, delim, std::back_inserter(elems));
        return elems;
    }

    static inline void ltrim(std::string &s)
    {
        s.erase(s.begin(),
                std::find_if(s.begin(), s.end(), [](int ch) {
                    return !std::isspace(ch);
                }));
    }

    static inline void rtrim(std::string &s)
    {
        s.erase(
            std::find_if(s.rbegin(), s.rend(), [](int ch) {
                return !std::isspace(ch);
            })
                .base(),
            s.end());
    }

    static inline void trim(std::string &s)
    {
        ltrim(s);
        rtrim(s);
    }

    static inline void removeNetworkTokens(std::string &s)
    {
        s.erase(s.begin());
        s.erase(s.end() - 1);
    }

    static inline std::string join(const std::vector<std::string> &vs, char delim)
    {
        std::stringstream ss;
        for (int i = 0; i < vs.size(); i++)
        {
            ss << vs[i];
            // If not last add delimiter
            if (i != vs.size() - 1)
                ss << delim;
        }
        return ss.str();
    }
}; // namespace string_utils

#endif // __STRING_UTILS_H_
